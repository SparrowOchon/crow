# Crow

The following theme was made based off of QT Creator's Dark theme and has been replicated to the JetBrains Suite. (Works with all products in the Suite).

#### C++

![c++](images/imagec++.cleaned.png)

#### Json

![json](images/imagejson.cleaned.png)

#### Html

![html](images/imagehtml.cleaned.png)

# Licensed:

[Show don't Sell License Version 1](https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md)

- Copyright (c) 2019, Network Silence
- All rights reserved.
